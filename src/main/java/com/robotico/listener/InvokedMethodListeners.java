package com.robotico.listener;

import java.sql.Connection;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import com.robotico.lib.ApplicationUtil;
import com.robotico.lib.SQLConnectionHelper;

public class InvokedMethodListeners implements IInvokedMethodListener {

    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println("Before Invocation of: " + method.getTestMethod().getMethodName() + "of Class:" + testResult.getTestClass());
        System.out.println("Before Invocation of: " + testResult.getMethod().getDescription()+ "of Class:" + testResult.getTestClass());
        Connection conn = SQLConnectionHelper.getEtonShcemaDBConnection();
        ApplicationUtil.insertTestExecutionStatus(conn, System.getProperty("cycle_scenario_table_id"), testResult.getMethod().getDescription(), "I");
    }

    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println("After Invocation of: " + method.getTestMethod().getMethodName() + "of Class:" + testResult.getTestClass());
        System.out.println("After Invocation of: " + method.getTestMethod().getDescription() + "of Class:" + testResult.getTestClass());
    }
}
