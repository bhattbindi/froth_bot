package com.robotico.listener;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewStyle;
import com.robotico.lib.MainUtil;

public class ExtentManager {
	private static ExtentReports extent;
	public static ExtentSparkReporter sparkReporter;

	/**
	 * Returns instance of extent reports
	 * 
	 * @param fileName Enter the Report or file name
	 * @return Returns the ExtentReports object
	 */
	/*
	 * public static ExtentReports createInstance(String fileName) {
	 * ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
	 * 
	 * htmlReporter.config().setTheme(Theme.STANDARD);
	 * htmlReporter.config().setReportName("Test Automation Report");
	 * htmlReporter.config().setDocumentTitle("robotico");
	 * htmlReporter.config().setChartVisibilityOnOpen(true);
	 * htmlReporter.config().setProtocol(Protocol.HTTPS);
	 * htmlReporter.config().setEncoding("UTF-8");
	 * htmlReporter.config().setTimeStampFormat("MM/dd/yyyy hh:mm:ss a");
	 * 
	 * extent = new ExtentReports(); extent.attachReporter(htmlReporter);
	 * 
	 * return extent; }
	 */

	public static ExtentReports createInstance(String fileName) {
		sparkReporter = new ExtentSparkReporter(fileName, ViewStyle.SPA);
		sparkReporter.config().setDocumentTitle("Automation Report");
		sparkReporter.config().setReportName("Automation Execution Report");
		sparkReporter.config().setTheme(Theme.DARK);
		sparkReporter.setAnalysisStrategy(AnalysisStrategy.SUITE);

		extent = new ExtentReports();
		extent.attachReporter(sparkReporter);
		extent.setSystemInfo("Environment", System.getProperty("env"));
		extent.setSystemInfo("Browser", System.getProperty("browser"));
		extent.setSystemInfo("Site ID", MainUtil.SITE_ID);
		extent.setSystemInfo("Tenant URL",
				MainUtil.siteDetails.get("tenant_url") == null
						|| MainUtil.siteDetails.get("tenant_url").equalsIgnoreCase("") ? ""
								: MainUtil.siteDetails.get("tenant_url"));
		extent.setSystemInfo("Client Name",
				MainUtil.siteDetails.get("client_name") == null
						|| MainUtil.siteDetails.get("client_name").equalsIgnoreCase("") ? ""
								: MainUtil.siteDetails.get("client_name"));
		extent.setSystemInfo("Client URL",
				MainUtil.siteDetails.get("client_url") == null
						|| MainUtil.siteDetails.get("client_url").equalsIgnoreCase("") ? ""
								: MainUtil.siteDetails.get("client_url"));

		return extent;
	}

	/*
	 * public static ExtentReports appendToExisting(String fileName) { sparkReporter
	 * = new ExtentSparkReporter(fileName);
	 * sparkReporter.config().setDocumentTitle("Automation Report");
	 * sparkReporter.config().setReportName("Automation Execution Report");
	 * sparkReporter.config().setTheme(Theme.DARK);
	 * sparkReporter.setAnalysisStrategy(AnalysisStrategy.SUITE); extent = new
	 * ExtentReports(); extent.attachReporter(sparkReporter);
	 * extent.setSystemInfo("Environment", System.getProperty("env"));
	 * extent.setSystemInfo("Browser", System.getProperty("browser")); return
	 * extent; }
	 */
}
