package com.robotico.listener;

import static com.robotico.lib.MainUtil.scenarioDetails;
import static com.robotico.lib.MainUtil.siteDetails;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.robotico.lib.MainUtil;
import com.robotico.lib.SQLConnectionHelper;

public class ExtentTestNGITestListener extends TestListenerAdapter {
	private static String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
	public static String USER_DIR = System.getProperty("user.dir");
	public static String table_id = System.getProperty("table_id");
	public static String executionId = System.getProperty("execution_id") == null
			|| System.getProperty("execution_id").equalsIgnoreCase("") ? dateName : System.getProperty("execution_id");
	private static String reportDirectoryName = "extentreport_" + executionId;

	private static ExtentReports extent;
	private static Logger logger = LogManager.getLogger(ExtentTestNGITestListener.class);
	public static String htmlpassLocation;
	public static String htmlfailLocation;
	public static String htmlreportname;

	static String reportL;

	private static void getExtent() {
		if (extent == null) {

			try {
				MainUtil.siteDetails = SQLConnectionHelper.getSiteDetails();
				System.out.println(MainUtil.siteDetails.toString());
				if (!System.getProperty("location").equalsIgnoreCase("locale"))
					SQLConnectionHelper.getEton_rei_dataDBConnBasedOnSites();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (System.getProperty("reporttype").equalsIgnoreCase("localr")) {

				htmlpassLocation = "images/Pass";
				htmlfailLocation = "images/Fail";

				htmlreportname = reportDirectoryName + "/" + "robotico" + "_" + executionId + ".html";
				logger.info("{}/{}", USER_DIR, htmlreportname);

				extent = ExtentManager.createInstance(USER_DIR + "/" + htmlreportname);

				extent.flush();
				reportL = "R";
				logger.info("USER DIRECTORY ISS----> {}", USER_DIR);
				SQLConnectionHelper.InsertToExecutionDetails();
				SQLConnectionHelper.UpdateToInInExecutionDetails("InProgress");

			} else {
				htmlpassLocation = "./images/Pass";
				htmlfailLocation = "./images/Fail";
				htmlreportname = reportDirectoryName + "/" + "robotico" + "_" + executionId + ".html";
				logger.info("{}/{}", USER_DIR, htmlreportname);
				extent = ExtentManager.createInstance(USER_DIR + "/" + htmlreportname);
				extent.flush();
				SQLConnectionHelper.InsertToExecutionDetails();
				SQLConnectionHelper.UpdateToInInExecutionDetails("InProgress");

			}

			if (System.getProperty("location").equalsIgnoreCase("locale"))
				SQLConnectionHelper.InsertToExecutions();
		}
	}

	private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<>();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();

	public static File passLocation = new File("./" + reportDirectoryName + "/images/Pass");
	public static File failLocation = new File("./" + reportDirectoryName + "/images/Fail");

	public ExtentTestNGITestListener() {

		if (!failLocation.exists()) {
			failLocation.mkdirs();
		}
		if (!passLocation.exists()) {
			passLocation.mkdirs();
		}

	}

	@Override
	public synchronized void onStart(ITestContext context) {
		System.out.println("*********************** ONSTART CONN SET *************************");
		try {
			MainUtil.delete4mDirectory(new File(USER_DIR), 15, "extentreport_");
			MainUtil.delete4mDirectory(new File(USER_DIR + MainUtil.file_separator + "src" + MainUtil.file_separator
					+ "test" + MainUtil.file_separator + "resources"), 15, "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Context Name: " + context.getName());
		MainUtil.scenarioDetails.put("Scenario", context.getName().replace("\\s+", " "));
		getExtent();

		String moduleName = SQLConnectionHelper.getModuleName(context.getName().replace("\\s+", " "));
		ExtentTest parent = extent
				.createTest((moduleName.equalsIgnoreCase("") ? "" : moduleName + "_") + context.getName());
		parentTest.set(parent);

	}

	@Override
	public synchronized void onFinish(ITestContext context) {
		extent.flush();
	}

	@Override
	public synchronized void onTestStart(ITestResult result) {

		try {

			String scenario = MainUtil.scenarioDetails.get("Scenario");

			if (!(scenario == null || scenario.equalsIgnoreCase("")))
				scenarioDetails = SQLConnectionHelper.readFrmBaseDataBasedOnScenario(scenario, "basedata_template_ntp");

			System.out.println("Recahed here in login");
			System.out.println(scenarioDetails.toString());
			System.out.println(siteDetails.toString());

			if (siteDetails.size() == 0 || scenarioDetails.size() == 0) {
				System.out
						.println("There is no data either in basedata_template_ntp or sitedetails Table with site id :"
								+ MainUtil.SITE_ID + " and Scenario :" + MainUtil.scenarioDetails.get("Scenario"));

			}

			System.out.println(result.getName() + " has started.");
			String value = result.getMethod().getDescription();

			if (value == null || value.equalsIgnoreCase("")) {
				System.out.println(result.getName() + " has started.");
			} else {
				ExtentTest child = parentTest.get().createNode(value).assignCategory(value);
				test.set(child);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * ExtentTest parent = extent.createTest(value); parentTest.set(parent);
		 */

	}

	// Pass type: applcation|description|jiraid
	public static synchronized void createNode(String type) {

		MainUtil.testcaseName = type;
		ExtentTest child = null;
		String jiracode = "";
		if (type.contains(":")) {
			String[] jiracodeno = type.split(":");

			if (jiracodeno[1].contains(",")) {
				String[] jeracodeList = jiracodeno[1].split(",");
				for (int i = 0; i < jeracodeList.length; i++) {
					jiracode += "<a target='_blank' href=\"https://www.google.com/\">" + jeracodeList[i].trim()
							+ "</a>,";
				}
				jiracode = StringUtils.removeEnd(jiracode, ",");
				type = jiracodeno[0] + " : " + jiracode;
				child = parentTest.get().createNode(type);
				for (int i = 0; i < jeracodeList.length; i++) {
					child.assignCategory(jeracodeList[i]);
				}
			} else {
				jiracode += "<a target='_blank' href=\"https://www.google.com/\">" + jiracodeno[1] + "</a>";
				type = jiracodeno[0] + " : " + jiracode;
				child = parentTest.get().createNode(type);
				child.assignCategory(jiracode);
			}
		} else
			child = parentTest.get().createNode(type);

		test.set(child);
	}

	public static synchronized void createDescryption(String descryption) {
		getTest().get().info(MarkupHelper.createLabel(descryption, ExtentColor.WHITE));
	}

	/*
	 * @Override public synchronized void onTestSuccess(ITestResult result) {
	 * test.get().pass("PASS", ExtentScreenCapture.captureSrceenPass(result)); }
	 *
	 *
	 * @Override public synchronized void onTestFailure(ITestResult result) {
	 * test.get().fail("FAIL", ExtentScreenCapture.captureSrceenFail(result)); }
	 *
	 * @Override public synchronized void onTestSkipped(ITestResult result) {
	 * test.get().skip(result.getThrowable()); }
	 */

	@Override
	public synchronized void onTestFailure(ITestResult result) {
		// test.get().fail("FAIL", ExtentScreenCapture.captureSrceenFail(result, null));
		// getTest().get().fail("" + result.getName());

	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {
		System.out.println("The name of the testcase Skipped is :" + result.getName());
		try {
			getParentTest().get().skip(MarkupHelper.createLabel(result.getName(), ExtentColor.ORANGE));
			extent.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/*
	 * @Override public synchronized void
	 * onTestFailedButWithinSuccessPercentage(ITestResult result) { // Not used yet
	 * }
	 */

	public static ThreadLocal<ExtentTest> getParentTest() {
		return parentTest;
	}

	public static ThreadLocal<ExtentTest> getTest() {
		extent.flush();
		return test;
	}

}
