package com.robotico.restassure;

import static io.restassured.RestAssured.given;

import java.util.List;
import java.util.Map;

import com.robotico.lib.MainUtil;
import com.robotico.listener.ExtentTestNGITestListener;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class Generic extends MainUtil {
	public static String URL;
	public static String restFileName;
	public static String restFileNameTemp;
	public static String basicAuthString;
	public static String input;
	public static String output;
	public static Headers headers;

	public static void addHeaders(List<Header> headerList) {
		try {
			Header h1 = new Header("Accept", "*/*");
			headerList.add(h1);
			headers = new Headers(headerList);
		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestNGITestListener.getTest().get().fail("Scenario is not available in DB");
		}
	}

	public static void addHeadersWithoutAccept(List<Header> headerList) {
		try {
			headers = new Headers(headerList);
		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestNGITestListener.getTest().get().fail("Scenario is not available in DB");
		}
	}

	public static void setBaseURI(String url) {

		URL = url;
		RestAssured.baseURI = URL;
		RestAssured.useRelaxedHTTPSValidation();
		System.out.println("API URL : " + URL);
	}

	public static Response postRequestWithFormParams(Map reqValue, String apiPath) {
		Response response = null;
		try {

			response = given().headers(headers).formParams(reqValue).post(apiPath).then().extract().response();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static Response getRequest(String apiPath) {
		Response response = null;
		try {

			response = given().headers(headers).get(apiPath).then().extract().response();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static Response getRequestWithQueryParams(Map<String, String> jsonObj, String apiPath) {
		Response response = null;
		try {

			response = given().headers(headers).queryParams(jsonObj).get(apiPath).then().extract().response();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static Response postRequestWithBody(String reqValue, String apiPath) {
		Response response = null;
		try {

			response = given().headers(headers).body(reqValue).post(apiPath).then().extract().response();
			response.prettyPrint();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
