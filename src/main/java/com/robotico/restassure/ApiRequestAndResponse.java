package com.robotico.restassure;

import static io.restassured.RestAssured.given;

import java.io.File;

import io.restassured.response.Response;

public class ApiRequestAndResponse extends Generic {

	public Response POSTRequest(File requestFile, String initialPath) {
		Response response = null;
		try {
			response = given().headers(headers).body(requestFile).post(initialPath).then().extract().response();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public Response GETRequest(String initialPath) {
		Response response = null;
		try {
			response = given().headers(headers).get(initialPath).then().extract().response();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
