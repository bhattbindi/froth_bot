package com.robotico.restassure;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import com.robotico.lib.MainUtil;
import com.robotico.listener.ExtentTestNGITestListener;

public class JaxWsLoggingHandler {
	public static File requestFile;
	public static File responseFile;
	// private static File file;

	private static File requestTextFile;
	private static File responseTextFile;
	private static FileWriter fr;
	private static BufferedWriter br;
	private static BufferedImage bufferedImage;
	private static Graphics graphics;
	private static int lineHeight;
	private static int yPos;

	public static void writeToRequestFile() {
		try {

			requestTextFile = new File(
					ExtentTestNGITestListener.passLocation + MainUtil.file_separator + "restApifiles");
			if (!requestTextFile.exists()) {
				requestTextFile.mkdir();
			}
			requestTextFile = new File(ExtentTestNGITestListener.passLocation + MainUtil.file_separator + "restApifiles"
					+ MainUtil.file_separator + Generic.restFileName + "_Request.txt");

			/*
			 * requestFile = new File( ExtentTestNGITestListener.passLocation +
			 * File.separator + Generic.restFileName + "_Request.png");
			 */

			fr = new FileWriter(requestTextFile);
			br = new BufferedWriter(fr);
			bufferedImage = new BufferedImage(1200, 1200, BufferedImage.TYPE_INT_RGB);
			graphics = bufferedImage.getGraphics();
			graphics.fillRect(0, 0, 1200, 1200);
			graphics.setColor(Color.BLACK);
			// graphics.setFont(graphics.getFont().deriveFont(15f));
			graphics.setFont(new Font("Calibri", Font.BOLD, 15));
			lineHeight = graphics.getFontMetrics().getHeight();

			yPos = 20;

			if (Generic.input != null) {

				fr.write(Generic.input.replaceAll(",", "," + System.getProperty("line.separator")));
			}
			fr.flush();
			fr.close();
			/*
			 * String xmlString; xmlString = Generic.input; String[] imagestring =
			 * xmlString.split(","); for (int i = 0; i < imagestring.length; i++) { int xPos
			 * = 10; yPos = yPos + lineHeight; graphics.drawString(imagestring[i], xPos,
			 * yPos); }
			 * 
			 * try { ImageIO.write(bufferedImage, "png", requestFile); } catch (IOException
			 * e1) { e1.printStackTrace(); }
			 */
			String path = ExtentTestNGITestListener.htmlpassLocation + File.separator + "restApifiles" + File.separator
					+ Generic.restFileName + "_Request.txt";
			ExtentTestNGITestListener.getTest().get()
					.pass("Request- " + "<a href='file:" + path + " '>" + Generic.restFileName + "_Request.txt</a>");
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void writeToResponseFile() {
		try {

			responseFile = new File(ExtentTestNGITestListener.passLocation + "/restApifiles");
			responseTextFile = new File(ExtentTestNGITestListener.passLocation + "/restApifiles" + File.separator
					+ Generic.restFileName + "_Response.txt");

			/*
			 * responseFile = new File( ExtentTestNGITestListener.passLocation +
			 * File.separator + Generic.restFileName + "_Response.png");
			 */

			fr = new FileWriter(responseTextFile);
			br = new BufferedWriter(fr);
			bufferedImage = new BufferedImage(1200, 1200, BufferedImage.TYPE_INT_RGB);
			graphics = bufferedImage.getGraphics();
			graphics.fillRect(0, 0, 1200, 1200);
			graphics.setColor(Color.BLACK); //
			graphics.setFont(graphics.getFont().deriveFont(15f));
			graphics.setFont(new Font("Calibri", Font.BOLD, 15));
			lineHeight = graphics.getFontMetrics().getHeight();

			yPos = 20;

			fr.write(Generic.output.replaceAll(",", "," + System.getProperty("line.separator")));

			fr.flush();
			fr.close();
			/*
			 * String xmlString; xmlString = Generic.output; String[] responseImagestring =
			 * xmlString.split("\n"); for (int i = 0; i < responseImagestring.length; i++) {
			 * int xPos = 10; // yPos = yPos*i;
			 * 
			 * yPos = yPos + lineHeight; graphics.drawString(responseImagestring[i], xPos,
			 * yPos); }
			 * 
			 * try { ImageIO.write(bufferedImage, "png", responseFile); } catch (IOException
			 * e1) { e1.printStackTrace(); }
			 */
			String path = ExtentTestNGITestListener.htmlpassLocation + File.separator + "restApifiles" + File.separator
					+ Generic.restFileName + "_Response.txt";
			ExtentTestNGITestListener.getTest().get()
					.pass("Response- " + "<a href='file:" + path + " '>" + Generic.restFileName + "_Response.txt</a>");
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}