package com.robotico.pageprop.HarUtil;

import java.util.List;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarPage;

public class HarUtil extends PageProperties {

    long totalPageSize = 0;
    long totalPageSize_Headers = 0;
    long totalPageLoadTime = 0;
    public double totalPageLoadTime_decimal = 0;
    public double totalPageSize_decimal = 0;
    public long totalPageLoadTime_Blocked;
    public long totalPageLoadTime_Connect;
    public long totalPageLoadTime_DNS;
    public long totalPageLoadTime_Send;
    public long totalPageLoadTime_Receive;
    public long totalPageLoadTime_SSL;
    public long totalPageLoadTime_Wait;

    public void harParser(Har har) {
        try {
            System.out.println("Har Parser");
            List<HarEntry> entries = har.getLog().getEntries();
            List<HarPage> pages = har.getLog().getPages();
            long startTime = pages.get(0).getStartedDateTime().getTime();
            System.out.println("page start time: " + startTime);
            long loadTime = 0;
            int entryIndex = 0;
            for (HarPage page : pages) {
                System.out.println("Title = " + page.getTitle());
                setPageName(page.getTitle());
            }
            System.out.println("#################################################################################");
            for (HarEntry entry : entries) {
                try {
                    String method = entry.getRequest().getMethod();
                    if (method.equals("GET")) {
                        if (entry.getResponse() != null) {
                            Long bodySize = entry.getResponse().getBodySize();
                            Long headerSize = entry.getResponse().getHeadersSize();
                            totalPageSize += bodySize;
                            totalPageSize_Headers += headerSize;
                        }
                        long blocked = entry.getTimings().getBlocked();
                        long getConnect = entry.getTimings().getConnect();
                        long getDNS = entry.getTimings().getDns();
                        long getReceive = entry.getTimings().getReceive();
                        long getSend = entry.getTimings().getSend();
                        long getSSL = entry.getTimings().getSsl();
                        long getWait = entry.getTimings().getWait();
                        if (blocked != -1)
                            totalPageLoadTime_Blocked += entry.getTimings().getBlocked();
                        if (getConnect != -1)
                            totalPageLoadTime_Connect += entry.getTimings().getConnect();
                        if (getDNS != -1)
                            totalPageLoadTime_DNS += entry.getTimings().getDns();
                        if (getReceive != -1)
                            totalPageLoadTime_Receive += entry.getTimings().getReceive();
                        if (getSend != -1)
                            totalPageLoadTime_Send += entry.getTimings().getSend();
                        if (getSSL != -1)
                            totalPageLoadTime_SSL += entry.getTimings().getSsl();
                        if (getWait != -1)
                            totalPageLoadTime_Wait += entry.getTimings().getWait();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                long entryLoadTime = entry.getStartedDateTime().getTime() + entry.getTime();
                if (entryLoadTime > loadTime) {
                    loadTime = entryLoadTime;
                }
                entryIndex++;
            }
            long loadTimeSpan = loadTime - startTime;
            Double webLoadTime = ((double) loadTimeSpan) / 1000;
            double webLoadTimeInSeconds = round(webLoadTime); // Math.round(webLoadTime * 100.0) / 100.0;
            totalPageLoadTime = totalPageLoadTime_Blocked + totalPageLoadTime_Connect + totalPageLoadTime_DNS + totalPageLoadTime_Receive + totalPageLoadTime_Send + totalPageLoadTime_SSL + totalPageLoadTime_Wait;
            totalPageSize_decimal = round((double) totalPageSize / 1024);
            totalPageLoadTime_decimal = webLoadTimeInSeconds;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("In Cache block");
        }

    }

    public static double round(double a) {
        double roundOff = Math.round(a * 100.0) / 100.0;
        return roundOff;
    }

}