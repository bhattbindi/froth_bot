package com.robotico.pageprop.HarUtil;

public class PageProperties {

	private String pageName;
	private String URL;
	private Long totalPageLoadTime;
	private Long totalPageSize;
	private Long pageLoadTimeBlocked;
	private Long pageLoadTimeConnect;
	private Long pageLoadTimeDNS;
	private Long pageLoadTimeSent;
	private Long pageLoadTimeReceive;
	private Long pageLoadTimeSSL;
	private Long pageLoadTimeWait;
	
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public Long getTotalPageLoadTime() {
		return totalPageLoadTime;
	}
	public void setTotalPageLoadTime(Long totalPageLoadTime) {
		this.totalPageLoadTime = totalPageLoadTime;
	}
	public Long getTotalPageSize() {
		return totalPageSize;
	}
	public void setTotalPageSize(Long totalPageSize) {
		this.totalPageSize = totalPageSize;
	}
	public Long getPageLoadTimeBlocked() {
		return pageLoadTimeBlocked;
	}
	public void setPageLoadTimeBlocked(Long pageLoadTimeBlocked) {
		this.pageLoadTimeBlocked = pageLoadTimeBlocked;
	}
	public Long getPageLoadTimeConnect() {
		return pageLoadTimeConnect;
	}
	public void setPageLoadTimeConnect(Long pageLoadTimeConnect) {
		this.pageLoadTimeConnect = pageLoadTimeConnect;
	}
	public Long getPageLoadTimeDNS() {
		return pageLoadTimeDNS;
	}
	public void setPageLoadTimeDNS(Long pageLoadTimeDNS) {
		this.pageLoadTimeDNS = pageLoadTimeDNS;
	}
	public Long getPageLoadTimeSent() {
		return pageLoadTimeSent;
	}
	public void setPageLoadTimeSent(Long pageLoadTimeSent) {
		this.pageLoadTimeSent = pageLoadTimeSent;
	}
	public Long getPageLoadTimeReceive() {
		return pageLoadTimeReceive;
	}
	public void setPageLoadTimeReceive(Long pageLoadTimeReceive) {
		this.pageLoadTimeReceive = pageLoadTimeReceive;
	}
	public Long getPageLoadTimeSSL() {
		return pageLoadTimeSSL;
	}
	public void setPageLoadTimeSSL(Long pageLoadTimeSSL) {
		this.pageLoadTimeSSL = pageLoadTimeSSL;
	}
	public Long getPageLoadTimeWait() {
		return pageLoadTimeWait;
	}
	public void setPageLoadTimeWait(Long pageLoadTimeWait) {
		this.pageLoadTimeWait = pageLoadTimeWait;
	}
	
}
