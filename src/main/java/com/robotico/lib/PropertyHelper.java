package com.robotico.lib;

public class PropertyHelper {
	/*
	 * private static HikariDataSource allConfigProperties;
	 * 
	 * private static Logger logger = LogManager.getLogger(PropertyHelper.class);
	 * static { allConfigProperties = setMySqlConnDSForProp(); }
	 * 
	 * private static HikariDataSource setMySqlConnDSForProp() {
	 * logger.info("calling setMySqlConnDSForProp");
	 * 
	 * String roboticoDbIp; if
	 * (System.getProperty("location").equalsIgnoreCase("local")) roboticoDbIp =
	 * "13.229.77.118"; else roboticoDbIp = "172.31.35.19"; String roboticoDbPort =
	 * "3306"; String roboticoDbSchema = "eton"; String roboticoDbUsername =
	 * "kumar"; String roboticoDbPassword = "Satheesh@123";
	 * 
	 * logger.info("calling setMySqlConnDS"); HikariConfig config = new
	 * HikariConfig(); HikariDataSource hikariDataSource;
	 * config.setDriverClassName("com.mysql.cj.jdbc.Driver");
	 * config.setJdbcUrl("jdbc:mysql://" + roboticoDbIp + ":" + roboticoDbPort + "/"
	 * + roboticoDbSchema + "?autoReconnect=true&useSSL=false");
	 * config.setMinimumIdle(0); config.setMaximumPoolSize(100);
	 * config.setUsername(roboticoDbUsername);
	 * config.setPassword(roboticoDbPassword);
	 * config.addDataSourceProperty("cachePrepStmts", "true");
	 * config.addDataSourceProperty("prepStmtCacheSize", "250");
	 * config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
	 * hikariDataSource = new HikariDataSource(config); return hikariDataSource;
	 * 
	 * }
	 * 
	 *//**
		 * This method is used to get the properties from config properties
		 *
		 * @param propertyName Pass the property name
		 * @return String
		 */
	/*
	 * public static String getProperties(String propertyName) {
	 * 
	 * String propValue = null; try (Connection conn = (allConfigProperties == null
	 * || allConfigProperties.isClosed()) ? setMySqlConnDSForProp().getConnection()
	 * : allConfigProperties.getConnection()) { PreparedStatement preparedStatement
	 * = conn.prepareStatement(
	 * "SELECT property_value FROM config_property_lists WHERE property_name=? AND env_name is null"
	 * ); preparedStatement.setString(1, propertyName); //
	 * preparedStatement.setString(2, "null"); ResultSet resultSet =
	 * preparedStatement.executeQuery(); while (resultSet.next()) { propValue =
	 * resultSet.getString("property_value"); } } catch (SQLException e) {
	 * logger.error("Error while fetching the property", e); } return propValue;
	 * 
	 * }
	 * 
	 *//**
		 * This method is used to get the properties from environment properties
		 *
		 * @param propertyName Pass the property name
		 * @return propertyName
		 */
	/*
	 * public static String getENVProperties(String propertyName) { String propValue
	 * = null; try (Connection conn = (allConfigProperties == null ||
	 * allConfigProperties.isClosed()) ? setMySqlConnDSForProp().getConnection() :
	 * allConfigProperties.getConnection()) { PreparedStatement preparedStatement =
	 * conn.prepareStatement(
	 * "SELECT property_value FROM config_property_lists WHERE property_name=? AND env_name=?"
	 * ); preparedStatement.setString(1, propertyName);
	 * preparedStatement.setString(2, MainUtil.ENVIRONMENT.toLowerCase()); ResultSet
	 * resultSet = preparedStatement.executeQuery(); while (resultSet.next()) {
	 * propValue = resultSet.getString("property_value"); } } catch (SQLException e)
	 * { logger.error("Error while fetching the property", e); } return propValue; }
	 * 
	 *//**
		 * This method is used to get the available hub url from the hub_details table
		 * with status "0"
		 *
		 * @return a String
		 * @throws Exception Will through exception
		 */
	/*
	 * public static String getHubUrl() throws Exception { String dataValue = null;
	 * String id = null; // SQLConnectionHelper.stepNewDBcloseConnection(); try
	 * (Connection con = SQLConnectionHelper.getRoboticoDBConnection()) {
	 * logger.info("Get the data from testdata DB"); String query; query =
	 * "select id,hub_url from auto_hub_details where hub_status='0' limit 1"; try
	 * (PreparedStatement stmt1 = con.prepareStatement(query)) { logger.info(query);
	 * try (ResultSet rs1 = stmt1.executeQuery()) { while (rs1.next()) { id =
	 * rs1.getString("id"); dataValue = rs1.getString("hub_url"); } } }
	 * MainUtil.storeVariable.put("HUB_URL_ID", id); } catch (SQLException e) {
	 * logger.error("SQL Exception: ", e); throw e; } return dataValue;
	 * 
	 * }
	 * 
	 */}
