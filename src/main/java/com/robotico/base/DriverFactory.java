package com.robotico.base;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.robotico.lib.SQLConnectionHelper;

import net.lightbody.bmp.BrowserMobProxy;

public class DriverFactory {

	private static List<WebDriverThread> webDriverThreadPool = Collections
			.synchronizedList(new ArrayList<WebDriverThread>());

	private static ThreadLocal<WebDriverThread> driverThread;

//	private static Logger logger = LogManager.getLogger(DriverFactory.class);

	protected static Map<String, RemoteWebDriver> driverPool;

	@BeforeSuite(alwaysRun = true)
	public static void instantiateDriverObject() {
//		logger.info("Instantiating Driver Object");
		File dir = new File("extentreport");
		dir.mkdir();
		driverThread = ThreadLocal.withInitial(() -> {
			WebDriverThread webDriverThread = new WebDriverThread();
			webDriverThreadPool.add(webDriverThread);
			return webDriverThread;
		});
	}

	/**
	 * Returns the remote web driver
	 *
	 * @param browserName  Provide the browser for storing in the driver pool (Ex:
	 *                     chrome1, chrome2)
	 * @param designatedVM Pass the VM name on which this execution should happen
	 * @return returns the remotewebdriver
	 */

	public static RemoteWebDriver getDriver(String browserName) {
//		logger.info("Getting WebDriver");
		driverThread.get().getDriver(browserName);
		return getDriverPool().get(browserName);
	}

	/**
	 * Returns the mobile driver from the driver pool
	 *
	 * @param appName     Provide the app name
	 * @param appPackage  Provide the appPackage name
	 * @param appActivity Provide appActivity
	 * @return returns the driver from the driver pool
	 */
	public static RemoteWebDriver getDriver(String appName, String appPackage, String appActivity, String deviceName) {
		driverThread.get().getDriver(appName, appPackage, appActivity, deviceName);
		return getDriverPool().get(appName);
	}

	public static RemoteWebDriver getDriver(String appName, String uuid, String bundleId, String xcodeOrgId,
			int timeout) {
		driverThread.get().getDriver(appName, "", "", "", 50);
		return getDriverPool().get(appName);
	}

	/**
	 * Get driver from driver pool stored in Map
	 *
	 * @return driver pool
	 */
	public static Map<String, RemoteWebDriver> getDriverPool() {
		if (driverPool == null) {
			driverPool = new HashMap<>();
		}
		return driverPool;
	}

	/*
	 * @AfterMethod public static void clearCookies() { try { if
	 * (!getDriverPool().entrySet().isEmpty()) { for (Map.Entry<String,
	 * RemoteWebDriver> driver : getDriverPool().entrySet()) { if
	 * (!((driver.getValue() instanceof AndroidDriver) || ((driver.getValue()
	 * instanceof IOSDriver)))) driver.getValue().manage().deleteAllCookies(); } } }
	 * catch (WebDriverException wde) {
	 * logger.error("No need of clearing the cookies"); }
	 * 
	 * }
	 */

	public static BrowserMobProxy getProxy() {
		return driverThread.get().getProxy();
	}

	/*
	 * @AfterClass public void afterMethod(ITestResult result) { TestResult
	 * testResult = null; if (result.getStatus() == result.SUCCESS) { testResult =
	 * TestResultFactory.createSuccess(); } else if (result.getStatus() ==
	 * result.FAILURE) { testResult =
	 * TestResultFactory.createFailure(result.getThrowable()); }
	 * WebDriverThread.reportiumClient.testStop(testResult);
	 * WebDriverThread.driver.close(); driver.quit(); }
	 */

	@AfterSuite(alwaysRun = true)
	public static void closeDriverObjects() {
		System.out.println("Close the Driver of method ***************");
		try {

			// logger.info("Updating the RemoteWebdriver ***************");
			if (webDriverThreadPool != null) {
				for (WebDriverThread webDriverThread : webDriverThreadPool) {
					webDriverThread.quitDriver();
				}
			} else {
				// logger.info("Driver already closed");
				System.out.println("Driver is already closed..");
			}

			driverThread.remove();
			SQLConnectionHelper.UpdateToInInExecutionDetails("Completed");

			SQLConnectionHelper.closeDBConnPool();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}