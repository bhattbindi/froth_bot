package com.robotico.base;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.robotico.lib.MainUtil;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

public class WebDriverThread {

	private BrowserMobProxy proxy;
	private static String workspace = System.getProperty("user.dir");
	private RemoteWebDriver driver;
	private static Logger logger = LogManager.getLogger(WebDriverThread.class);
	private static final String CHROME_DRIVER_STR = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_STR = "webdriver.gecko.driver";

	static {
		java.util.logging.Logger.getLogger("org.apache.http.wire").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.hc.client5.http.headers").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.hc.client5.http.wire").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger("com.zaxxer.hikari.pool.HikariPool").setLevel(Level.OFF);

		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "ERROR");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "ERROR");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "ERROR");
	

	}

	/**
	 * Driver instantiation module for WEB
	 *
	 * @param browserName Name of the driver ex: CHROME1,CHROME2,FIREFOX1
	 * @return Return the initiated driver
	 */
	RemoteWebDriver getDriver(String browserName) {
		// String sauceUserName = System.getenv("SAUCE_USERNAME");
		// String sauceAccessKey = System.getenv("SAUCE_ACCESS_KEY");
		/*
		 * String sauceUserName = "prabathkumar"; String sauceAccessKey =
		 * "c78680af-4acc-42e4-b250-196360e2e3cc";
		 */ String sauceUserName = "roboticodigital";
		String sauceAccessKey = "3c3dcc3add0f4a37a414a98c033b6322";
		System.setProperty("username", "roboticodigital");
		System.setProperty("accessKey", "3c3dcc3add0f4a37a414a98c033b6322");

		logger.info(System.getProperty("os.name"));
		try {
			if (DriverFactory.getDriverPool().get(browserName) == null) {
				if (System.getProperty("location").contains("local")) {
					if ("firefox".equalsIgnoreCase(browserName)) {
						if (System.getProperty("os.name").toLowerCase().contains("windows"))
							System.setProperty(FIREFOX_DRIVER_STR,
									Paths.get(workspace, "src", "main", "resources", "geckodriver.exe").toString());
						else
							System.setProperty(FIREFOX_DRIVER_STR,
									Paths.get(workspace, "src", "main", "resources", "geckodriver").toString());

						DesiredCapabilities BrowserCapabilities = DesiredCapabilities.firefox();

						BrowserCapabilities.setCapability("marionette", true);

						FirefoxOptions opt = new FirefoxOptions();
						opt.merge(BrowserCapabilities);
						WebDriverManager.firefoxdriver().setup();
						driver = new FirefoxDriver(opt);
					} else if ("chrome".equalsIgnoreCase(browserName)) {
						/*
						 * if (System.getProperty("os.name").toLowerCase().contains("windows"))
						 * System.setProperty(CHROME_DRIVER_STR, Paths.get(workspace, "src", "main",
						 * "resources", "chromedriver.exe").toString()); else { if
						 * (System.getProperty("location").equalsIgnoreCase("local"))
						 * System.setProperty(CHROME_DRIVER_STR, Paths.get(workspace, "src", "main",
						 * "resources", "chromedriver").toString()); else
						 * System.setProperty(CHROME_DRIVER_STR, "/usr/bin/chromedriver"); }
						 */
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
						chromePrefs.put("download.default_directory", MainUtil.downlaodfilepath);
						chromePrefs.put("download.prompt_for_download", Boolean.valueOf(false));

						ChromeOptions chromeOptions = new ChromeOptions();
						System.out.println("reached here");
						if ("headless".equals(System.getProperty("state")))
							chromeOptions.addArguments("--headless");
						chromeOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						chromeOptions.setAcceptInsecureCerts(true);
						chromeOptions.addArguments("ignore-certificate-errors");
						chromeOptions.addArguments("disable-popup-blocking");
						chromeOptions.addArguments("start-maximized");
						chromeOptions.setExperimentalOption("prefs", chromePrefs);
						/*
						 * chromeOptions.addExtensions( new
						 * File("E:\\WORKSPACES\\ROBOTICO_WORKSPACE\\xmls\\extension.crx"));
						 */

						/*
						 * chromeOptions.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");
						 * 
						 * chromeOptions.addArguments("--output", "html");
						 * chromeOptions.addArguments("--output-path","./GoogleHome.html");
						 */
						if (System.getProperty("pmas").equalsIgnoreCase("true"))
							chromeOptions.setProxy(startProxy());

						WebDriverManager.chromedriver().setup();
						driver = new ChromeDriver(chromeOptions);
						// driver = new RemoteWebDriver(new URL("http://127.0.0.1:4651/wd/hub"),
						// chromeOptions);
						driver.manage().deleteAllCookies();
					}
				} else if (System.getProperty("location").equalsIgnoreCase("remote")) {
					if ("firefox".equals(System.getProperty("browser"))) {
						FirefoxOptions firefoxOptions = new FirefoxOptions().setProfile(new FirefoxProfile());
						firefoxOptions.setAcceptInsecureCerts(true);
						if (System.getProperty("pmas").equalsIgnoreCase("true"))
							firefoxOptions.setProxy(startProxy());
						System.setProperty("webdriver.gecko.driver",
								Paths.get(workspace, "src", "main", "resources", "geckodriver.exe").toString());

						System.out.println("Going to initialise firefox");
						driver = new RemoteWebDriver(new URL("http://172.31.22.51:4545/wd/hub"), firefoxOptions);
						driver.manage().window().maximize();
					} else if ("chrome".equals(System.getProperty("browser"))) {
						if (System.getProperty("os.name").toLowerCase().contains("windows"))
							System.setProperty(CHROME_DRIVER_STR,
									Paths.get(workspace, "src", "main", "resources", "chromedriver.exe").toString());
						else
							System.setProperty(CHROME_DRIVER_STR, "/usr/bin/chromedriver");

						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", Integer.valueOf(0));
						chromePrefs.put("download.default_directory", MainUtil.downlaodfilepath);
						chromePrefs.put("download.prompt_for_download", Boolean.valueOf(false));

						ChromeOptions chromeOptions = new ChromeOptions();

						if ("headless".equals(System.getProperty("state")))
							chromeOptions.addArguments("--headless");

						chromeOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
								UnexpectedAlertBehaviour.ACCEPT);
						chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						chromeOptions.addArguments("ignore-certificate-errors");
						chromeOptions.addArguments("disable-popup-blocking");
						chromeOptions.addArguments("start-maximized");

						chromeOptions.addArguments("--test-type");
						chromeOptions.addArguments("test-type");
						chromeOptions.addArguments("start-maximized");
						chromeOptions.addArguments("--window-size=1920,1080");
						chromeOptions.addArguments("--enable-precise-memory-info");
						chromeOptions.addArguments("--disable-popup-blocking");
						chromeOptions.addArguments("--disable-default-apps");
						chromeOptions.addArguments("test-type=browser");
						chromeOptions.addArguments("--incognito");
						chromeOptions.addArguments("--no-sandbox");

						chromeOptions.setBinary("/usr/bin/google-chrome");
						chromeOptions.setExperimentalOption("prefs", chromePrefs);

						if (System.getProperty("pmas").equalsIgnoreCase("true"))
							chromeOptions.setProxy(startProxy());

						// MainUtil.HubUrl = PropertyHelper.getHubUrl();
						MainUtil.HubUrl = "http://172.31.14.184:4545/wd/hub";
						/*
						 * if (MainUtil.HubUrl.equalsIgnoreCase("") || MainUtil.HubUrl == null) {
						 * logger.error("Please makesure the node is up and avaialble in DB."); } else {
						 */
						WebDriverManager.chromedriver().setup();
						driver = new RemoteWebDriver(new URL(MainUtil.HubUrl), chromeOptions);
						// PropertyHelper.updateHubUrlStatus(MainUtil.storeVariable.get("HUB_URL_ID"),
						// "1");
						logger.info("The Execution started in this " + MainUtil.HubUrl + "Machine.");
						// }

					}
				} else {

					System.out.println("Other than chrome and firefox and remote and local");

					MutableCapabilities sauceOpts = new MutableCapabilities();
					sauceOpts.setCapability("username", sauceUserName);
					sauceOpts.setCapability("accessKey", sauceAccessKey);
					sauceOpts.setCapability("seleniumVersion", "3.141.59");
					sauceOpts.setCapability("name", "Test");
					sauceOpts.setCapability("screenResolution", "1440x900");
					List<String> tags = Arrays.asList("sauceDemo", "demoTest", "module4");
					sauceOpts.setCapability("tags", tags);
					sauceOpts.setCapability("maxDuration", 1080);
					sauceOpts.setCapability("commandTimeout", 600);
					sauceOpts.setCapability("idleTimeout", 1000);
					sauceOpts.setCapability("build", System.getProperty("buildNo"));
					sauceOpts.setCapability("prerun",
							"https://gist.githubusercontent.com/sweetasubudhi02/d17ce732511acb401febeb405e36689e/raw/23aa09bb673a857d4cb5a5e13051a57ccfc28d49/windowsHost.bat");
					sauceOpts.setCapability("background", false);
					sauceOpts.setCapability("timeout", 120);

					ChromeOptions chromeOpts = new ChromeOptions();
					chromeOpts.setExperimentalOption("w3c", true);
					chromeOpts.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
							UnexpectedAlertBehaviour.ACCEPT);
					chromeOpts.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					chromeOpts.setAcceptInsecureCerts(true);
					MutableCapabilities capabilities = new MutableCapabilities();
					capabilities.setCapability("sauce:options", sauceOpts);
					capabilities.setCapability("goog:chromeOptions", chromeOpts);
					capabilities.setCapability("browserName", "chrome");
					capabilities.setCapability("platformName", "Windows 10");
					capabilities.setCapability("browserVersion", "84");

					driver = new RemoteWebDriver(new URL("https://" + System.getProperty("username") + ":"
							+ System.getProperty("accessKey") + "@ondemand.eu-central-1.saucelabs.com:443/wd/hub"),
							capabilities);
				}
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				driver.manage().deleteAllCookies();
			}
			DriverFactory.getDriverPool().put(browserName, driver);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		} catch (

		Exception e) {
			logger.error("Error creatings the driver", e);
		}
		return driver;
	}

	/**
	 * Driver instantiation module for mobile (Android)
	 *
	 * @param appName     Provide the mobile app name
	 * @param appPackage  Provide the mobile app package name
	 * @param appActivity Provide the app activity name
	 * @param deviceName  Provide the node name where it is has to be run
	 * @return Return the initiated driver
	 */
	RemoteWebDriver getDriver(String appName, String appPackage, String appActivity, String deviceName) {
		String location = System.getProperty("location");
		DesiredCapabilities androidDcap = DesiredCapabilities.android();

		if (location.equalsIgnoreCase("local")) {
			/*
			 * DesiredCapabilities caps = DesiredCapabilities.android();
			 * caps.setCapability("app",
			 * "https://www.dropbox.com/s/z10xwjxpb9zm7yr/istaff-android.apk?dl=0");
			 * caps.setCapability("username", "demousermano1");
			 * caps.setCapability("accessKey", "0742de9b-20c0-4806-808a-f383cfb4ef");
			 * caps.setCapability("appiumVersion", "1.18.1");
			 * caps.setCapability("deviceName", deviceName);
			 * caps.setCapability("deviceOrientation", "portrait");
			 * caps.setCapability("browserName", ""); caps.setCapability("platformVersion",
			 * "9.0"); caps.setCapability("platformName", "Android");
			 * caps.setCapability("app", "sauce-storage:istaff-android.apk"); try {
			 * WebDriver driver = new AndroidDriver<WebElement>( new URL(
			 * "https://demousermano1:0742de9b-20c0-4806-808a-f383cfb4ef0f@ondemand.saucelabs.com:443/wd/hub"
			 * ), caps); } catch (Exception e) { e.printStackTrace(); }
			 */
		} else if (location.equalsIgnoreCase("browserstack")) {
			String username = System.getProperty("username");
			String accessKey = System.getProperty("accessKey");
			String buildName = System.getenv("BROWSERSTACK_BUILD_NAME");
			String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
			String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");

			androidDcap.setCapability("device", "Google Pixel 3");
			androidDcap.setCapability("os_version", "9.0");
			androidDcap.setCapability("project", "My First Project");
			androidDcap.setCapability("build", "My First Build");
			androidDcap.setCapability("name", "Bstack-[Java] Sample Test");
			androidDcap.setCapability("app", "bs://bf80acf415df2d15982a38f9061ec4ff7055eb0a");
			androidDcap.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");

			androidDcap.setCapability("build", buildName); // CI/CD job name using
			androidDcap.setCapability("browserstack.local", browserstackLocal);
			androidDcap.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
			System.out.println("browserstack.local" + browserstackLocal);
			System.out.println("browserstack.localIdentifier" + browserstackLocalIdentifier);

		} else {
			androidDcap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
			androidDcap.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
			androidDcap.setCapability("username", System.getProperty("username"));
			androidDcap.setCapability("accessKey", System.getProperty("accessKey"));
			androidDcap.setCapability("deviceName", deviceName);
			androidDcap.setCapability("appiumVersion", "1.18.1");
			androidDcap.setCapability("noReset", "false");
			androidDcap.setCapability("deviceOrientation", "portrait");
			androidDcap.setCapability("browserName", "");
			androidDcap.setCapability("platformVersion", "10");
			androidDcap.setCapability("platformName", "Android");
			androidDcap.setCapability("app", "sauce-storage:istaff-android.apk");
			// androidDcap.setCapability("app",
			// "storage:2bf5d23b-d398-4860-9519-d35231de0cc7");
			androidDcap.setCapability("build", System.getProperty("buildNo"));
			androidDcap.setCapability("newCommandTimeout", 1500);
			try {
				driver = new AndroidDriver<>(new URL(
						"https://prabathkumar:c78680af-4acc-42e4-b250-196360e2e3cc@ondemand.eu-central-1.saucelabs.com:443/wd/hub"),
						androidDcap);
				logger.info("Session ID of Android: " + driver.getSessionId());
			} catch (WebDriverException e) {
				logger.error("Driver instantiating failed or app is not installed", e);
			} catch (MalformedURLException e) {
				logger.error(e);
			}
		}
		DriverFactory.getDriverPool().put(appName, driver);
		return driver;
	}

	RemoteWebDriver getDriver(String appName, String uuid, String bundleId, String xcodeOrgId, int timeout) {
		DesiredCapabilities ioscaps = DesiredCapabilities.iphone();
		try {
			if (System.getProperty("location").equalsIgnoreCase("local")) {
				ioscaps.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, 8100);
				ioscaps.setCapability(IOSMobileCapabilityType.BUNDLE_ID, bundleId);
				ioscaps.setCapability(IOSMobileCapabilityType.XCODE_ORG_ID, xcodeOrgId);
				ioscaps.setCapability(IOSMobileCapabilityType.XCODE_SIGNING_ID, "iPhone Developer");
				ioscaps.setCapability("automationName", "XCUITest");
				ioscaps.setCapability("platformName", "iOS");
				ioscaps.setAcceptInsecureCerts(true);
				ioscaps.acceptInsecureCerts();
				ioscaps.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 60000);
				ioscaps.setCapability(IOSMobileCapabilityType.COMMAND_TIMEOUTS, 60000);
				ioscaps.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, 1200000);
				ioscaps.setCapability(IOSMobileCapabilityType.WDA_CONNECTION_TIMEOUT, 1200000);
				driver = new IOSDriver<>(new URL("http://127.0.0.1:4651/wd/hub"), ioscaps);
			} else if (System.getProperty("location").equalsIgnoreCase("iossim")) {
				ioscaps.setCapability("appiumVersion", "1.18.1");
				ioscaps.setCapability("deviceName", "iPhone XR Simulator");
				ioscaps.setCapability("deviceOrientation", "portrait");
				ioscaps.setCapability("platformVersion", "12.4");
				ioscaps.setCapability("platformName", "iOS");
				ioscaps.setCapability("browserName", "");
				ioscaps.setCapability("app", "https://github.com/prakashpqa/demoproject/raw/master/ssp.zip");
				driver = new IOSDriver<>(new URL(
						"https://prabathkumar:c78680af-4acc-42e4-b250-196360e2e3cc@ondemand.eu-central-1.saucelabs.com:443/wd/hub"),
						ioscaps);
			} else {
				/*
				 * ioscap.setCapability("testobject_api_key",
				 * System.getProperty("testobject_api_key"));
				 * ioscap.setCapability("testobject_app_id",
				 * System.getProperty("testobject_app_id"));
				 * ioscap.setCapability("appiumVersion", "1.18.1");
				 * ioscap.setCapability("automationName", "XCUITest");
				 * ioscap.setCapability("platformName", "iOS"); ioscap.setCapability("name",
				 * "robotico Test"); ioscap.setCapability("platformVersion", "13.5.1");
				 * ioscap.setCapability("deviceName", "iPhone_8_13_real");
				 * ioscap.setCapability("newCommandTimeout", 120);
				 * ioscap.setCapability("wdaEventloopIdleDelay", "5"); driver = new
				 * IOSDriver<>(new URL("https://eu1.appium.testobject.com/wd/hub"), ioscap);
				 */
				MutableCapabilities sauceOptions = new MutableCapabilities();
				sauceOptions.setCapability("name", "iOS Demo Tests");
				sauceOptions.setCapability("tags", "iostest");
				sauceOptions.setCapability("build", System.getProperty("buildNo"));
				sauceOptions.setCapability("username", System.getProperty("username"));
				sauceOptions.setCapability("accessKey", System.getProperty("accessKey"));
//                ioscaps.setCapability("username", System.getProperty("username"));
//                ioscaps.setCapability("accessKey", System.getProperty("accessKey"));
				ioscaps.setCapability("sauce:options", sauceOptions);
				ioscaps.setCapability("deviceName", "iPhone_8_13_real");
				ioscaps.setCapability("deviceOrientation", "portrait");
				ioscaps.setCapability("platformVersion", "13.5.1");
				ioscaps.setCapability("platformName", "iOS");
				ioscaps.setCapability("browserName", "");
				ioscaps.setCapability("startIWDP", Boolean.TRUE);
				ioscaps.setCapability("eventLoopIdleDelaySec", "4");
				ioscaps.setCapability("wdaEventloopIdleDelay", "7");
				ioscaps.setCapability("waitForQuiescence", Boolean.FALSE);
				ioscaps.setCapability("waitForQuietness", Boolean.FALSE);
				ioscaps.setCapability("build", System.getProperty("buildNo"));
				ioscaps.setCapability("app", "sauce-storage:WMA.ipa");
				driver = new IOSDriver<>(new URL(
						"https://prabathkumar:c78680af-4acc-42e4-b250-196360e2e3cc@ondemand.eu-central-1.saucelabs.com:443/wd/hub"),
						ioscaps);

			}
		} catch (Exception e) {
			logger.error("IOSDriver Driver instantiating failed", e);
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		DriverFactory.getDriverPool().put(appName, driver);
		return driver;
	}

	BrowserMobProxy getProxy() {
		return this.proxy;
	}

	private Proxy startProxy() {
		logger.info("Proxy configuration");
		logger.info("Starting the proxy");
		proxy = new BrowserMobProxyServer();
		proxy.setTrustAllServers(true);
		proxy.start(4444);
		logger.info("get the Selenium proxy object - org.openqa.selenium.Proxy");
		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
		String hostIp = null;
		try {
			hostIp = Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		seleniumProxy.setHttpProxy(hostIp + ":" + proxy.getPort());
		seleniumProxy.setSslProxy(hostIp + ":" + proxy.getPort());
		return seleniumProxy;
	}

	/*
	 * @AfterSuite public void insertVideoURL(){ // Retrieve the URL to the
	 * DigitalZoom Report String reportURL = reportiumClient.getReportUrl();
	 * System.out.println(reportURL); }
	 */

	/**
	 * Quit driver
	 */
	void quitDriver() {

		/*
		 * This lines of code is throwing null pointer exception for webdriver which
		 * prakash has implementedfor myyes4g execution
		 */

		/*
		 * try { if (this.service.isRunning()) { service.stop();
		 * this.deviceApi.releaseDevice(this.deviceSerial); } try {
		 * this.deviceApi.releaseDevice(this.deviceSerial); } catch (Exception e) {
		 * logger.error("Error changing the status of the device", e); }
		 */
		try {
			logger.info("quitDriver");
			if (!DriverFactory.getDriverPool().entrySet().isEmpty()) {
				for (Map.Entry<String, RemoteWebDriver> driverInstance : DriverFactory.getDriverPool().entrySet()) {
					if (driverInstance.getValue() != null || !driverInstance.getValue().equals("")) {
						if ((driverInstance.getValue() instanceof AndroidDriver)
								|| (driverInstance.getValue() instanceof IOSDriver))
							try {
								driverInstance.getValue().quit();
							} catch (WebDriverException wb) {
								logger.info("Session is already terminated");
							}
						else if (driverInstance.getValue() instanceof RemoteWebDriver) {
							try {
								driverInstance.getValue().quit();
							} catch (WebDriverException wb) {
								logger.info("Session is already terminated");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception in closing the driver \n", e);
		}
	}
}
